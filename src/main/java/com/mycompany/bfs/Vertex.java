/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bfs;

/**
 *
 * @author informatics
 */
public class Vertex {

    public char label; // label (e.g. ‘A’)
    public boolean wasVisited;
// -------------------------------------------------------------

    public Vertex(char lab) // constructor
    {
        label = lab;
        wasVisited = false;
    }
}
